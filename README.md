#### Day 2 > Activity 4

## User Registration API

From the current user registration flow:

1. Create API for to register a user
    1. Enpoint: `/register`
    2. Response must contain the `verification_code`
    3. Request and response must have `reference-number`
2. Create API to verify user registration
    1. Endpoint: `/verify`
    2. Request and response must have `reference-number`
3. Create API for login
    1. Endpoint: `/login`
    2. Fields are `email` and password `password`
    3. Request and response must have `reference-number`
    4. Response must contain `lastLogin`: ISO format date and time 